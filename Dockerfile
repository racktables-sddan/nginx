# 
#
# Author: Samuel Dan
# Contact: samuel.d.dan@gmail.com

# Base image
FROM nginx:latest

# Install nginx-full
RUN apt-get update && apt-get install nginx-full -y

# Copy source files
COPY nginx.conf /etc/nginx/nginx.conf
COPY racktables.conf /etc/nginx/conf.d/default.conf

# Expose webservice port
EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]
